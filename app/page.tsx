import Footer from "@/src/components/Footer";
import Navbar from "@/src/components/Navbar";
import '../src/scss/App.scss'
import Image from "next/image";

import drapeau from '../src/assets/img/svg/flag/drapeau_france.svg'
import tototte1 from '../src/assets/img/svg/bento/tototte1.svg'
import moustache from '../src/assets/img/svg/bento/moustache.svg'
import tut from '../src/assets/img/svg/bento/tut.svg'
import tot from '../src/assets/img/svg/tot/tot.svg'

import red from '../src/assets/img/svg/pod/red.svg'
import pink from '../src/assets/img/svg/pod/pink.svg'
import green from '../src/assets/img/svg/pod/green.svg'
import chelou from '../src/assets/img/svg/pod/chelou.svg'
import bacon from '../src/assets/img/svg/pod/bacon.svg'
import france from '../src/assets/img/svg/pod/france.svg'
import enfant from '../src/assets/img/svg/pod/enfant.svg'
import fruit from '../src/assets/img/svg/pod/fruit.svg'
export default function Home() {
  
  return (
    <>
    <Navbar/>
      <div className="Home_Container">
        <div className="Home_Content">
          <div className="video-container">
            <video
              className="video"
              src="https://thefrenchtototte.net/dev/herovideo.mp4"
              autoPlay
              muted
              loop
            />
          </div>
          <div className="subtitle">
            <h2>the french tototte</h2>
            <Image src={drapeau} alt="" />
            <h1>Pour ceux qui osent être différents</h1>
            <p>Découvrir</p>
          </div>
          <div className="angry-grid">
            <div className="bento">
              <div id="item-0">
                <Image src={tototte1} alt=""/>
                <div className="text">
                  <h3>élégante et sophistiquée</h3>
                  <p>Design raffiné et ornements exclusifs pour une expérience de vapotage distinguée et élégante</p>
                </div>
              </div>
              <div id="item-1">
                <div className="text">
                  <h3>personnalisable</h3>
                  <p>Ornements et accessoires premium pour créer une vapoteuse unique qui reflète votre style personnel</p>
                </div>
              </div>
              <div id="item-2">
                  <Image src={moustache} alt=""/>
                  <div className="text">
                    <h3>made in france</h3>
                    <p>Savoir-faire français pour une expérience de suçotage authentique et exceptionnelle</p>
                  </div>
              </div>
              <div id="item-3">
                  <Image src={tut} alt="" width={480} height={339}/>
                  <div className="text">
                    <h3>Innovante</h3>
                    <p>Technologies de pointe offrant une expérience de suçotage révolutionnaire et inégalée.</p>
                  </div>
              </div>
              <div id="item-4">
                  <div className="text">
                    <h3>Savoureuse</h3>
                    <p>Palette de saveurs exquises pour une expérience de suçotage délicieusement satisfaisante et immersive.</p>
                  </div>
              </div>
              <div id="item-5">
                   <Image src={drapeau} alt=""/>
                  <div className="text">
                    <h3>hébergement 100% français</h3>
                    <p>Notre site web est hébergé en france grâce à notre partenaire o2switch</p>
                  </div>
              </div>
            </div>
          </div>
          <div className="tech">
            <div className="img">
              <Image src={tot} alt=""/>
              
            </div>
            <div className="text">
                <div className="title">
                  <h1>technologie plug’n’tote</h1>
                </div>
                <div className="desc">
                  <p>
                  La tototte est équipée de la <span>technologie plug&#39;n&#39;tote</span>, permettant de changer de plug à volonté, offrant ainsi un accès à <span>une gamme infinie de saveurs de suçotage</span> pour satisfaire tous les goûts.
                  </p>
                  <p className="deux">
                  Cette technologie vous permet de changer en un clin d’œil de saveur pour <span>voyager du bout des lèvres.</span>
                  </p>
                </div>
            </div>
            <p className="dec">Découvrir les saveur</p>
          </div>
          <div className="tototte">
            <div className="img">
            <div className="img1">
              <Image src={red} alt="" />
              <div className="text">
              <h2>Fraise</h2>
              <p >Une fraise française cueilli dans no chers régions</p>
              </div>
            </div>
            <div className="img2">
              <Image src={pink} alt="" />
                <div className="text">
                  <h2>Menthe</h2>
                  <p>
                  pour une haleine fraiche avant vos réunions
                  </p>
                </div>
            </div>
            <div className="img3">
              <Image src={green} alt="" />
              <div className="text">
                  <h2>Bubble Gomme</h2>
                  <p>un goût sucré pour retourner en enfance</p>
                </div>
            </div>
            <div className="img4">
              <Image src={chelou} alt="" />
              <div className="text">
                  <h2>spicy</h2>
                  <p>pour ceux qui aiment vivre des nouvelles chose tout les jours</p>
                </div>
            </div>
            <div className="img5">
              <Image src={bacon} alt="" />
              <div className="text">
                  <h2>Bacon</h2>
                  <p>un goût de porc pour honorer nos élevages made in france</p>
                </div>
            </div>
            <div className="img6">
              <Image src={france} alt="" />
              <div className="text">
                  <h2>france</h2>
                  <p>Suçotez la République avec cet étendar bleu blanc rouge</p>
                </div>
            </div>
            <div className="img7">
              <Image src={enfant} alt="" />
              <div className="text">
                  <h2>jus de mynthos</h2>
                  <p>profitez du gout concentré de la premiere traite du matin</p>
                </div>
            </div>
            <div className="img8">
              <Image src={fruit} alt="" />
              <div className="text">
                  <h2>montaza et torez</h2>
                  <p>pour retrouver le goût d&#39;un vin unique</p>
                </div>
            </div>
            </div>
            <div className="infos">
              <div className="trente">
                <span>30</span>
                <p>Saveurs disponibles</p>
              </div>
              <div className="onze">
                <span>11</span>
                <p>Vertus médicinales</p>
              </div>
              <div className="letexte">
                <p>Laissez-vous tenter par les saveurs des différents plugs et <strong>découvrez un monde de délices</strong> pour chaque envie.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
