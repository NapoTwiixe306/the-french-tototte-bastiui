import '../scss/components/Navbar.scss'
import logo from '../assets/img/svg/logo/logo.svg'
import Image from 'next/image'
export default function Navbar() {
    return (
        <>
            <div className="Navbar_Container">
                <div className="Navbar_Content">
                    <div className="logo">
                        <Image src={logo} alt='logo'/>
                    </div>
                    <div className="button">
                        <button>
                            Acheter
                        </button>
                    </div>

                </div>
            </div>
        </>
    )
}